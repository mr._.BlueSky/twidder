/**
 * Author:Özgür Özer
 */
var socket;
var token;
var email;
var myChart;
window.onload = function () {
  initstorage();
  token = localStorage.getItem('token');
  if (token.length === 70) {
    email = localStorage.getItem('email');
    var shaObj = new jsSHA('SHA-512', 'TEXT');
    shaObj.update(token + email);
    var hash = shaObj.getHash('HEX');
    var data = {
      email: email,
      hash: hash
    };
    socket = new WebSocket('ws://127.0.0.1:5000/echo');
    socket.onopen = function () {
      socket.send(JSON.stringify(data));
    };
    var profileview = document.querySelector('#profileview');
    document.body.innerHTML = profileview.innerHTML;
    handlerPro();
  } else {
    var welcomeView = document.querySelector('#welcomeview');
    document.body.innerHTML = welcomeView.innerHTML;
    handlerWel();
  }
};
function initstorage() {
  if (localStorage.getItem('token') === null) {
    localStorage.setItem('token', '');
  }
  if (localStorage.getItem('email') === null) {
    localStorage.setItem('email', '');
    localStorage.setItem('token', '');
  }
}
function handlerWel() {
  var sgnUp = document.forms.signup;
  sgnUp.addEventListener('submit', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    signUp(sgnUp);
  });
  var lgn = document.forms.login;
  lgn.addEventListener('submit', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    login(lgn);
  });
}
function handlerPro() {
  // Make chart displaying the quantity of members, signed in users, and posts
  var ctx = document.querySelector('#myChart').getContext('2d');
  myChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: [
        'Members',
        'Signed users',
        'Posts'
      ],
      datasets: [
        {
          label: 'Quantity',
          data: [
            0,
            0,
            0
          ],
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)'
          ],
          borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)'
          ],
          borderWidth: 1
        }
      ]
    },
    options: {
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true
            }
          }
        ]
      }
    }
  });
  // Home tab implementation
  var homeTab = document.querySelector('#homeTab');
  homeTab.addEventListener('click', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    make_visible('home');
  });
  // Fill about
  var userdata = new XMLHttpRequest();
  var shaObj = new jsSHA('SHA-512', 'TEXT');
  shaObj.update(token + email + 'GET');
  var hash = shaObj.getHash('HEX');
  userdata.open('GET', 'http://127.0.0.1:5000/get_user_data_by_token/' + email
  + '/' + hash, true);
  userdata.onreadystatechange = function () {
    if (userdata.readyState === 4) {
      if (userdata.status === 200) {
        var output = JSON.parse(userdata.responseText);
        //for global variable "email"
        email = output.email;
        var infoArea = {
          email: document.querySelector('#hmail'),
          firstname: document.querySelector('#hfirst_name'),
          familyname: document.querySelector('#hfamily_name'),
          gender: document.querySelector('#hgender'),
          country: document.querySelector('#hcountry'),
          city: document.querySelector('#hcity')
        };
        for (var field in output) {
          for (var area in infoArea) {
            if (field === area) {
              append_span(infoArea[area], output[field]);
            }
          }
        }
      } else if (userdata.status === 401) {
        console.error('user validation failed');
      }
    }
  };
  userdata.send();
  var msgForm = document.forms.hmsgpost;
  msgForm.ondrop = drop_handler;
  msgForm.ondragover = dragover_handler;
  // Post message on signed in user's wall
  msgForm.addEventListener('submit', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var message = e.target.message.value.trim();
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/post_message', true);
    var info = document.querySelector('#hmsgpost p');
    if (info === null) {
      var info = document.createElement('p');
      msgForm.appendChild(info);
    }
    var shaObj = new jsSHA('SHA-512', 'TEXT');
    shaObj.update(token + email + email + message);
    var hash = shaObj.getHash('HEX');
    var data = {
      'senderEmail': email,
      'email': email,
      'hash': hash,
      'message': message
    };
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        info.textContent = xhr.responseText;
        retrieve_messages();
        msgForm.message.value = '';
      }
    };
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(data));
  });
  // Post media on home wall
  var mediaForm = document.querySelector('#hmedia');
  mediaForm.addEventListener('submit', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var xhr = new XMLHttpRequest();
    var info = document.querySelector('#hmedia p');
    if (info === null) {
      var info = document.createElement('p');
      mediaForm.appendChild(info);
    }
    var shaObj = new jsSHA('SHA-512', 'TEXT');
    shaObj.update(token + email + email);
    var hash = shaObj.getHash('HEX');
    xhr.open('POST', '/upload_media/' + email + '/' + email + '/' + hash, true);
    xhr.onreadystatechange = function (e) {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          fetch_media();
        } else {
          info.textContent = xhr.responseText;
        }
      }
    };
    form = new FormData(e.target);
    xhr.send(form);
  });
  // reload button
  var reload = document.querySelector('#home article a');
  reload.addEventListener('click', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    retrieve_messages();
  });
  // Account Tab
  var accountTab = document.querySelector('#accountTab');
  accountTab.addEventListener('click', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    make_visible('account');
  });
  //Password change form
  var passwordChangeForm = document.forms.changepassword;
  passwordChangeForm.addEventListener('submit', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var oldPassword = passwordChangeForm.oldpassword.value;
    var newPassword = passwordChangeForm.newpassword.value;
    var repeat = passwordChangeForm.repeat.value;
    var info = document.querySelector('#changepassword p');
    if (info === null) {
      var info = document.createElement('p');
      passwordChangeForm.appendChild(info);
    }
    if (newPassword.length > 7 && oldPassword.length > 7) {
      if (newPassword !== repeat) {
        info.textContent = 'The passwords do not match!';
        oldPassword.value = '';
        newPassword.value = '';
        repeat.value = '';
      } else {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/change_password', true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.onreadystatechange = function () {
          if (xhr.readyState === 4) {
            info.textContent = xhr.responseText;
          }
        };
        var shaObj = new jsSHA('SHA-512', 'TEXT');
        shaObj.update(token + email + oldPassword + newPassword);
        var hash = shaObj.getHash('HEX');
        var input = {
          'email': email,
          'oldPassword': oldPassword,
          'newPassword': newPassword,
          'hash': hash
        };
        xhr.send(JSON.stringify(input));
      }
    } else {
      info.textContent = 'The password should be longer than 7!';
      oldPassword.value = '';
      newPassword.value = '';
      repeat.value = '';
    }
  });
  // Sign out
  var sgnOutBtn = document.querySelector('#signout');
  sgnOutBtn.addEventListener('click', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/sign_out', true);
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          var welcomeview = document.querySelector('#welcomeview');
          document.body.innerHTML = welcomeview.innerHTML;
          handlerWel();
          localStorage.setItem('token', '');
          localStorage.setItem('email', '');
          socket.close();
        }
      }
    };
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(email));
  });
  // Browse tab
  var browseTab = document.querySelector('#browseTab');
  browseTab.addEventListener('click', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    make_visible('browse');
    // First search area is shown if search is successful this view is shown
    var othersHome = document.querySelector('#othersHome');
    othersHome.style = 'display: none;';
  });
  //search user
  var oemail; // other person's email
  var searchForm = document.forms.searchform;
  searchForm.addEventListener('submit', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    oemail = searchForm.searchmail.value.trim();
    var info = document.querySelector('#searchform p');
    if (info === null) {
      var info = document.createElement('p');
      searchForm.appendChild(info);
    }
    var shaObj = new jsSHA('SHA-512', 'TEXT');
    shaObj.update(token + email + oemail + 'GET');
    var hash = shaObj.getHash('HEX');
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'get_user_data_by_email/' + email + '/' + oemail + '/'
    + hash, true);
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          var othersHome = document.querySelector('#othersHome');
          var search = document.querySelector('#search');
          search.style.display = 'none';
          othersHome.style = 'block';
          var header = document.querySelector('#otersInfo h4');
          header.innerHTML = 'About ' + oemail;
          var output = JSON.parse(xhr.responseText);
          var infoArea = {
            email: document.querySelector('#omail'),
            firstname: document.querySelector('#ofirst_name'),
            familyname: document.querySelector('#ofamily_name'),
            gender: document.querySelector('#ogender'),
            city: document.querySelector('#ocity'),
            country: document.querySelector('#ocountry')
          };
          var infos = document.querySelectorAll('#othersHome aside li span');
          for (var i = 0; i < infos.length; i++) {
            infos[i].remove();
          }
          for (var field in output) {
            for (var area in infoArea) {
              if (field === area) {
                append_span(infoArea[area], output[field]);
              }
            }
          }
          fetch_media(oemail);
          retrieve_messages(oemail);
        } else {
          info.textContent = xhr.responseText;
        }
      }
    };
    xhr.send();
  });
  var postForm = document.forms.omsgpost;
  postForm.ondrop = drop_handler;
  postForm.ondragover = dragover_handler;
  // Post message on other's wall
  postForm.addEventListener('submit', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var info = document.querySelector('#othersHome form p');
    if (info === null) {
      var info = document.createElement('p');
      postForm.appendChild(info);
    }
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/post_message', true);
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          info.textContent = (xhr.responseText);
          retrieve_messages(oemail);
        }
      }
    };
    var message = e.target.message.value.trim();
    var shaObj = new jsSHA('SHA-512', 'TEXT');
    shaObj.update(token + email + oemail.trim() + message);
    var hash = shaObj.getHash('HEX');
    xhr.setRequestHeader('Content-Type', 'application/json');
    var input = {
      'hash': hash,
      'senderEmail': email,
      'email': oemail.trim(),
      'message': message
    };
    xhr.send(JSON.stringify(input));
  });
  // Post media on others wall
  var postMedia = document.querySelector('#omedia');
  postMedia.addEventListener('submit', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var xhr = new XMLHttpRequest();
    var info = document.querySelector('#hmedia p');
    if (info === null) {
      var info = document.createElement('p');
      mediaForm.appendChild(info);
    }
    var shaObj = new jsSHA('SHA-512', 'TEXT');
    shaObj.update(token + email + oemail);
    var hash = shaObj.getHash('HEX');
    xhr.open('POST', '/upload_media/' + email + '/' + oemail + '/' + hash, true);
    xhr.onreadystatechange = function (e) {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          fetch_media(oemail);
        } else {
          info.textContent = xhr.responseText;
        }
      }
    };
    form = new FormData(e.target);
    xhr.send(form);
  });
  window.onpopstate = function () {
    hist_handler(token);
  };
  socket.onerror = function () {
    console.log('An error occured on socket');
  };
  //functions for single login interface implementation
  socket.onmessage = function (e) {
    /*when another login takes place the server sends the signal
         *"newConnection" causing the former session to shut down
         *with the exploitation of sign out button*/
    data = JSON.parse(e.data);
    if (data === 'newConnection') {
      sgnOutBtn.click();
    } /*When a change in number of members or signed in users arises the
         *server sends an array of new numbers at the same time implying a
         *change has occured*/

    if (Array.isArray(data) && !(myChart === undefined)) {
      refresh_chart(data);
    }
  };
  hist_handler(token);
  retrieve_messages();
  fetch_media();
  setTimeout(refresh_chart(null, false), 500);
}
function signUp(sgnUpForm) {
  initstorage();
  var info = document.querySelector('#signup p');
  if (info === null) {
    var info = document.createElement('p');
    sgnUpForm.appendChild(info);
  }
  if (sgnUpForm.signuppassword.value !== sgnUpForm.spasswordrepeat.value) {
    info.textContent = 'The passwords should be the same';
    sgnUpForm.signuppassword.value = '';
    sgnUpForm.spasswordrepeat.value = '';
  } else {
    var sgnUpObj = {
      email: sgnUpForm.signupmail.value.trim(),
      password: sgnUpForm.signuppassword.value,
      firstname: sgnUpForm.firstname.value.trim(),
      familyname: sgnUpForm.familyname.value.trim(),
      gender: sgnUpForm.gender.value.trim(),
      city: sgnUpForm.city.value.trim(),
      country: sgnUpForm.country.value.trim()
    };
    var xhr = new XMLHttpRequest();
    xhr.open('PUT', '/sign_up', true);
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        info.textContent = xhr.responseText;
      }
    };
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(sgnUpObj));
  }
}
function login(loginForm) {
  initstorage();
  var input = {
    'email': loginForm.loginmail.value.trim(),
    'password': loginForm.loginpassword.value
  };
  var formArea = document.querySelector('#login');
  var info = document.querySelector('#login p');
  if (info === null) {
    var info = document.createElement('p');
    formArea.appendChild(info);
  }
  var xhr = new XMLHttpRequest();
  xhr.open('PUT', '/sign_in', true);
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        token = JSON.parse(xhr.responseText);
        email = input.email;
        localStorage.setItem('token', token);
        localStorage.setItem('email', input.email);
        var shaObj = new jsSHA('SHA-512', 'TEXT');
        shaObj.update(token + email);
        var hash = shaObj.getHash('HEX');
        var data = {
          email: email,
          hash: hash
        };
        socket = new WebSocket('ws://127.0.0.1:5000/echo');
        socket.onopen = function () {
          socket.send(JSON.stringify(data));
        };
        var profileview = document.querySelector('#profileview');
        document.body.innerHTML = profileview.innerHTML;
        handlerPro();
      } else if (xhr.status === 401) {
        info.textContent = xhr.statusText;
      }
    }
  };
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send(JSON.stringify(input));
}
function hist_handler() {
  // history manipulation. This is simply make_visible function without pushing state
  if (history.state === 'account') {
    tab = 'account';
  } else if (history.state === 'browse') {
    tab = 'browse';
    var othersHome = document.querySelector('#othersHome');
    othersHome.style = 'display: none;';
    var search = document.querySelector('#search');
    search.style = 'display: block';
  } else {
    tab = 'home';
  }
  var tabs = {
    home: '#home',
    account: '#account',
    browse: '#browse'
  };
  for (var t in tabs) {
    if (t === tab) {
      document.querySelector(tabs[t]).style.display = 'inherit';
      document.querySelector(tabs[t] + 'Tab').style = 'background-color: #0A0A0A;';
    } else {
      document.querySelector(tabs[t]).style.display = 'none';
      document.querySelector(tabs[t] + 'Tab').style = 'background-color: inherit;';
    }
  }
}
function append_span(parent, text) {
  var span = document.createElement('span');
  span.textContent = text;
  parent.appendChild(span);
}
function append_li(parent, text) {
  var li = document.createElement('li');
  li.textContent = text.writer + ': ' + text.content;
  li.draggable = true;
  li.ondragstart = dragstart_handler;
  parent.appendChild(li);
} // Functions for drag and drop feature

function dragstart_handler(e) {
  e.dataTransfer.setData('text/plain', e.target.textContent);
  e.dataTransfer.effectAllowed = 'copy';
}
function dragover_handler(e) {
  e.preventDefault();
  e.dataTransfer.dropEffect = 'copy';
}
function drop_handler(e) {
  e.preventDefault();
  var data = e.dataTransfer.getData('text/plain');
  data = data.split(':') [1];
  e.target.textContent = data;
} /*live data presentation*/

function refresh_chart(data, noRequest = true) {
  // Update chart content. "noRequest"
  // specify whether to get the data to update fron server.
  if (noRequest) {
    myChart.data.datasets[0].data[1] = data[0];
    myChart.data.datasets[0].data[0] = data[1];
  } else {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', '/get_member_count', true);
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        var data = JSON.parse(xhr.responseText);
        myChart.data.datasets[0].data[1] = data[0];
        myChart.data.datasets[0].data[0] = data[1];
        myChart.update();
      }
    };
    xhr.send();
  }
}
function make_visible(tab) {
  var tabs = {
    home: '#home',
    account: '#account',
    browse: '#browse'
  };
  for (var t in tabs) {
    if (t === tab) {
      var state = tabs[t].slice(1);
      history.pushState(state, state, state);
      document.querySelector(tabs[t]).style.display = 'inherit';
      document.querySelector(tabs[t] + 'Tab').style = 'background-color: #A0A0A0;';
    } else {
      document.querySelector(tabs[t]).style.display = 'none';
      document.querySelector(tabs[t] + 'Tab').style = 'background-color: inherit;';
    }
  }
}
function retrieve_messages(oemail = null) {
  // Get the media list of the user with email "oemail" and place them
  // if no oemail is given, medias are retrieved for the logged in user
  var xhr = new XMLHttpRequest();
  var output = null;
  if (oemail === null) {
    var msgArea = document.querySelector('#hmsgArea');
    var presentMsgs = document.querySelectorAll('#hmsgArea li');
    var info = document.querySelector('#hmsgArea p');
    if (info === null) {
      var info = document.createElement('p');
      msgArea.appendChild(info);
    }
    var shaObj = new jsSHA('SHA-512', 'TEXT');
    shaObj.update(token + email + 'GET' + '/get_user_messages_by_token');
    var hash = shaObj.getHash('HEX');
    xhr.open('GET', '/get_user_messages_by_token/' + email + '/' + hash, true);
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          output = JSON.parse(xhr.responseText);
          if (output) {
            for (var i = 0; i < presentMsgs.length; i++) {
              msgArea.removeChild(presentMsgs[i]);
            }
            for (var msg in output) {
              append_li(msgArea, output[msg]);
            }
            var post = document.querySelectorAll('#hmsgArea li').length;
            var postsInGraph = myChart.data.datasets[0].data[2];
            if (postsInGraph !== post) {
              myChart.data.datasets[0].data[2] = post;
            }
            myChart.update();
          }
        } else if (xhr.status === 401) {
          info.textContent = xhr.responseText;
        }
      }
    };
  } else {
    var msgArea = document.querySelector('#msgs');
    var presentMsgs = document.querySelectorAll('#msgs li');
    var info = document.querySelector('#msgs p');
    if (info === null) {
      var info = document.createElement('p');
      msgArea.appendChild(info);
    }
    var shaObj = new jsSHA('SHA-512', 'TEXT');
    shaObj.update(token + email + oemail);
    var hash = shaObj.getHash('HEX');
    xhr.open('GET', '/get_user_messages_by_email/' + email + '/' + oemail + '/' + hash, true);
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          output = JSON.parse(xhr.responseText);
          for (var i = 0; i < presentMsgs.length; i++) {
            msgArea.removeChild(presentMsgs[i]);
          }
          for (var msg in output) {
            append_li(msgArea, output[msg]);
          }
        } else {
          info.textContent = xhr.responseText;
        }
      }
    };
  }
  xhr.send();
}
function fetch_media(oemail = '') {
  // Get the media list of the user with email "oemail" and place them.
  // if no oemail is given, medias are retrieved for the logged in user
  var forhome = false;
  if (!oemail) {
    oemail = email;
    forhome = true;
  }
  var xhr = new XMLHttpRequest();
  var shaObj = new jsSHA('SHA-512', 'TEXT');
  shaObj.update(token + email + oemail);
  var hash = shaObj.getHash('HEX');
  xhr.open('GET', '/get_media_list/' + email + '/' + oemail + '/' + hash, true);
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        if (forhome) {
          oemail = '';
        }
        var mediaObj = JSON.parse(xhr.response);
        put_media(oemail, mediaObj);
      }
    }
  };
  xhr.send();
}
function put_media(oemail = null, mediaObj) {
  // mediaObj is a an Object with 2 Arrays images and videos. oemail is just to
  // specify where to put he medias. If none is given for oemail, they are put on the 
  // current users wall
  if (oemail) {
    var vidArea = document.querySelector('#ovids');
    var imgArea = document.querySelector('#oimgs');
  } else {
    var imgArea = document.querySelector('#himgs');
    var vidArea = document.querySelector('#hvids');
  }
  imgArea.innerHTML = '';
  vidArea.innerHTML = '';
  if (mediaObj.images) {
    for (var img = 0; img < mediaObj.images.length; img++) {
      append_media(imgArea, mediaObj.images[img]);
    }
  }
  if (mediaObj.videos) {
    for (var vid = 0; vid < mediaObj.videos.length; vid++) {
      append_media(vidArea, mediaObj.videos[vid], true);
    }
  }
}
function append_media(parent, media, video = false) {
/*Append a video/img node with filename "media" to the "parent" node. If
it is video it appended as video, otherwise as image.*/
  var div = document.createElement('div');
  parent.appendChild(div);
  var header = document.createElement('h3');
  div.appendChild(header);
  var poster = Object.keys(media) [0];
  header.innerHTML = poster;
  var shaObj = new jsSHA('SHA-512', 'TEXT');
  shaObj.update(token + email + media[poster]);
  var hash = shaObj.getHash('HEX');
  var url = '/get_media/' + email + '/' + media[poster] + '/' + hash;
  if (video) {
    var vid = document.createElement('video');
    div.appendChild(vid);
    vid.setAttribute('buffered', '');
    vid.setAttribute('controls', '');
    vid.setAttribute('preload', 'auto');
    vid.setAttribute('src', url);
    vid.setAttribute('width', '600px');
  } else {
    var img = document.createElement('img');
    div.appendChild(img);
    //filename as the alternative text
    img.setAttribute('alt', media[poster]);
    img.setAttribute('src', url);
    img.setAttribute('width', '600px');
  }
}
