PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE accounts(
email TEXT PRIMARY KEY NOT NULL,
token TEXT,
password TEXT NOT NULL,
firstname TEXT NOT NULL,
familyname TEXT NOT NULL,
gender TEXT NOT NULL,
city TEXT NOT NULL,
country TEXT NOT NULL,
messages TEXT
);
COMMIT;
